#ifndef SCANING_MACRO_H_
#define SCANING_MACRO_H_

#include <fstream>

using namespace std;

#define MAX_STEP_CNT    12
#define MAX_DET_NO      8

class Scaning_Macro 
{
public: 
    Scaning_Macro(char *cfg_file);
    Scaning_Macro(int run_no, int trg_no, int det_no);
    int sm_run(void);
    int sm_run_cli(void);


private:
    int cfg_read(char *cfg_file);
    void cfg_print(void);
    int read_rootfile(void);

    ifstream input_cfg_file;
    int input_cfg_file_ok_flag;

    int run_nr;
    int bcids[20];
    int start_id;
    int end_id;
    int det_nr;
    int trg_nr;
    int bcid_cnt;
    int bcid_m2;
    int bcid_m1;
    int bcid_00;
    int bcid_p1;
    int bcid_p2;
    
    string scanning_steps[MAX_STEP_CNT] = {
        "default",
        "0.0",
        "2.50008",
        "5.00016",
        "7.50024",
        "10.00032",
        "12.5004",
        "15.00048",
        "17.50056",
        "20.00064",
        "22.50072",
        "24.89663",
    };

    const char *scanning_steps_labels[MAX_STEP_CNT] = {
        "def.",
        "0.0",
        "2.5",
        "5.0",
        "7.5",
        "10.0",
        "12.5",
        "15.0",
        "17.5",
        "20.0",
        "22.5",
        "25.0",
    };
};

#endif  /* SCANING_MACRO_H_ */