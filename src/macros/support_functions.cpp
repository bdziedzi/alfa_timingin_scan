#include "support_functions.h"

int bcid_to_string(int bcid, string *ret_string)
{
    if (bcid < 10) {
        *ret_string = "000";
        *ret_string += to_string(bcid);

        return 0;
    }

    if (bcid < 100) {
        *ret_string = "00";
        *ret_string += to_string(bcid);

        return 0;
    }

    if (bcid < 1000) {
        *ret_string = "0";
        *ret_string += to_string(bcid);

        return 0;
    }

    if (bcid > BCID_MAX) 
    {
        return 1;
    }

    *ret_string += to_string(bcid);
    return 0;
}