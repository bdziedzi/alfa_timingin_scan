#include "scaning_macro.h"

#include <iostream>
#include <string>

#include "TFile.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TCanvas.h"

// #include "plot.h"
#include "support_functions.h"

using namespace std;

/**
 * @brief Scaning macro class constructor
 *
 * @param[in] char *cfg_file -> path to the config file
 */
Scaning_Macro::Scaning_Macro(char *cfg_file)
{
    cout << "Scaning Macro v1.0" << endl;

    input_cfg_file_ok_flag = cfg_read(cfg_file);

    return;
}

Scaning_Macro::Scaning_Macro(int run_no, int trg_no, int det_no)
{
    cout << "Scaning Macro v1.1" << endl;

    run_nr = run_no;
    det_nr = det_no;
    trg_nr = trg_no;

    
    // input_cfg_file_ok_flag = cfg_read(cfg_file);

    return;
}

/**
 * @brief Function analysing the root file data
 */
int Scaning_Macro::read_rootfile(void)
{
    static int AllTracksReco_entries[MAX_STEP_CNT+2][MAX_DET_NO+2];
    static int BCXatROD_entries[MAX_STEP_CNT+2];

    for (int step_cnt = 0; step_cnt < MAX_STEP_CNT; step_cnt++) {
        
        // open root sourcefile
        string input_root_file;
        input_root_file += to_string(run_nr);
        input_root_file += "/r0000";
        input_root_file += to_string(run_nr);
        input_root_file += "_step_";
        input_root_file += scanning_steps[step_cnt];
        input_root_file += ".root";

        cout << "\ninput_root_file: " << input_root_file << endl;
    
        TFile input_root_data(input_root_file.c_str());
        
        // check if file is properly opened
        if (input_root_data.IsOpen()) {
            cout << "ROOT file properly opened." << endl;
        } else {
            cout << "ROOT file not opened properly. Skipping the file putting zeros in the result table..." << endl;
            for (int det_no = 1; det_no - 1 < MAX_DET_NO; det_no++) {
                AllTracksReco_entries[step_cnt][det_no] = 0;
            }
            BCXatROD_entries[step_cnt] = 0;
            continue;
        }

        // load data from the source root file AllTracksReco_entries
        for (int det_no = 1; det_no - 1 < MAX_DET_NO; det_no++) {
            string s_input_plot = "";
            s_input_plot = "Histogramming/gnamALFA/SHIFT/RP_";
            s_input_plot += to_string(det_no);
            // s_input_plot += "/BCXatMB";
            s_input_plot += "/AllTracksReco";
            cout << "ROOT file PATH: " << s_input_plot << endl;    
        
            TH2I *th2i_alltracksreco;
            th2i_alltracksreco = (TH2I*) input_root_data.Get(s_input_plot.c_str());
            
            if (th2i_alltracksreco == NULL) {
                AllTracksReco_entries[step_cnt][det_no] = 0;
            } else {
                AllTracksReco_entries[step_cnt][det_no] = th2i_alltracksreco->GetEntries();
            }
        }

        // load data from the source root file BCXatROD_entries
        {   
            string s_input_plot = "";
            s_input_plot = "Histogramming/gnamALFA/SHIFT/ALFA_common/BCXatROD_0";
            cout << "ROOT file PATH: " << s_input_plot << endl;
            // cout << "BCXatROD_entries[" << step_cnt << "]:" << endl;
            TH1I *th1i_bcxatrod;
            th1i_bcxatrod = (TH1I*) input_root_data.Get(s_input_plot.c_str());
            BCXatROD_entries[step_cnt] = th1i_bcxatrod->GetEntries();
            // cout << "Entries: " << BCXatROD_entries[step_cnt] << endl;

            // cout << "\t\t\tBCXatROD_entries[0] = " << BCXatROD_entries[0] << endl;
        }
    }

    // print values
    cout << "\n\n=====================================\n" << endl;

    // print header
    cout << "Run number: " << run_nr << "\n " << endl;

    // print AllTracksReco_entries  
    cout << "Entries: " << endl;

    cout << "\t";
    for (int step_cnt = 1; step_cnt < MAX_STEP_CNT; step_cnt++) {
        cout << "\t" << scanning_steps_labels[step_cnt];
    }
    cout << endl;

    // for (int det_no = 1; det_no - 1 < MAX_DET_NO; det_no++) {

        cout << "\tRP_" << det_nr << ": ";
        for (int step_cnt = 1; step_cnt < MAX_STEP_CNT; step_cnt++) {
            cout << "\t" << AllTracksReco_entries[step_cnt][det_nr];
        }
        cout << endl;
    // }
    // cout << endl;

    

    // print BCXatROD_entries
    cout << "BCX at ROD: ";// << endl;
    // cout << "\tAll:";
    for (int step_cnt = 1; step_cnt < MAX_STEP_CNT; step_cnt++) {
        //  cout << "\tstep " << scanning_steps_labels[step_cnt] << ":\t" << AllTracksReco_entries[step_cnt];
         cout << "\t" << BCXatROD_entries[step_cnt];
    }
    cout << endl;

    cout << "efficiency: ";// << endl;
    // cout << "\tAll:";
    for (int step_cnt = 1; step_cnt < MAX_STEP_CNT; step_cnt++) {
        //  cout << "\tstep " << scanning_steps_labels[step_cnt] << ":\t" << AllTracksReco_entries[step_cnt];
         cout << "\t" << (double) AllTracksReco_entries[step_cnt][det_nr] / (double) BCXatROD_entries[step_cnt];
    }
    cout << endl;
    cout << endl;

    return 0;
}

/**
 * @brief Function reading the scanning macro parameters from the config file.
 *
 * @param[in] char *cfg_file -> path to the config file
 */
int Scaning_Macro::cfg_read(char *cfg_file)
{
    string cfg_file_s = cfg_file;
    cout << "Config file name: " << cfg_file << endl;

    // open config file
    input_cfg_file.open(cfg_file_s, ifstream::in);

    // check if the file does exist
    if (input_cfg_file.good() == false) {
        cout << "Error! File " << cfg_file << " not found!\a" << endl;
        return 1;
    }

    cout << "Pointed file is properly opened!" << endl << endl;

    string tmp;

    while (!input_cfg_file.eof()) {
        input_cfg_file >> tmp;

        if (!tmp.compare("bcid_cnt")) {
            input_cfg_file >> tmp;
            bcid_cnt = stoi(tmp);
        }

        if (!tmp.compare("bcids"))
        {
            for (int i = 0; i < bcid_cnt; i++) {
                input_cfg_file >> tmp;
                bcids[i] = stoi(tmp);
            }
        }

        if (!tmp.compare("run_nr")) {
            input_cfg_file >> tmp;
            run_nr = stoi(tmp);
        }

        if (!tmp.compare("start_id")) {
            input_cfg_file >> tmp;
            start_id = stoi(tmp);
        }

        if (!tmp.compare("end_id")) {
            input_cfg_file >> tmp;
            end_id = stoi(tmp);
        }

        if (!tmp.compare("det_nr")) {
            input_cfg_file >> tmp;
            det_nr = stoi(tmp);
        }

        if (!tmp.compare("trg_nr")) {
            input_cfg_file >> tmp;
            trg_nr = stoi(tmp);
        }

        if (!tmp.compare("bcid_m2")) {
            input_cfg_file >> tmp;
            bcid_m2 = stoi(tmp);
        }

        if (!tmp.compare("bcid_m1")) {
            input_cfg_file >> tmp;
            bcid_m1 = stoi(tmp);
        }

        if (!tmp.compare("bcid_00")) {
            input_cfg_file >> tmp;
            bcid_00 = stoi(tmp);
        }

        if (!tmp.compare("bcid_p1")) {
            input_cfg_file >> tmp;
            bcid_p1 = stoi(tmp);
        }

        if (!tmp.compare("bcid_p2")) {
            input_cfg_file >> tmp;
            bcid_p2 = stoi(tmp);
        }
    }

    // close config file
    input_cfg_file.close();

    return 0;
}

/**
 * @brief Function printing the loaded scanning macro parameters
 */
void Scaning_Macro::cfg_print(void)
{
    cout << "Common configuration: " << endl;

    cout << "bcids[0] : " << bcids[0] << endl;
    cout << "bcids[1] : " << bcids[1] << endl;
    cout << "run_nr   : " << run_nr   << endl;
    cout << "start_id : " << start_id << endl;
    cout << "end_id   : " << end_id   << endl;
    cout << "det_nr   : " << det_nr   << endl;
    cout << "bcid_m2  : " << bcid_m2  << endl;
    cout << "bcid_m1  : " << bcid_m1  << endl;
    cout << "bcid_00  : " << bcid_00  << endl;
    cout << "bcid_p1  : " << bcid_p1  << endl;
    cout << "bcid_p2  : " << bcid_p2  << endl;
    
    cout << endl;
}

/**
 * @brief Function running analysis using read config parameters
 */
int Scaning_Macro::sm_run(void) 
{
    if (input_cfg_file_ok_flag) {
        return 1;
    }

    cfg_print();
    read_rootfile();

    return 0;
}

/**
 * @brief Function running analysis using read config parameters
 */
int Scaning_Macro::sm_run_cli(void) 
{
    read_rootfile();

    return 0;
}