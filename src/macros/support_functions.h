#ifndef SUPPORT_FUNCTIONS_H
#define SUPPORT_FUNCTIONS_H

#include <string>

using namespace std;

#define BCID_MAX    3654

int bcid_to_string(int bcid, string *ret_string);

#endif /* SUPPORT_FUNCTIONS_H */