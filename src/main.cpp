#include <iostream>

#include "macros/scaning_macro.h"

using namespace std;

int end_program(int err_code);

int main(int argc, char *argv[])
{
    int err_end = 0;
    cout << "ALFA FE timing-in scan analyzer v1.1" << endl;

    if (argc == 1) {
        Scaning_Macro scanning_macro(argv[1]);
        int sm_err = scanning_macro.sm_run();
        if (sm_err) {
            cout << "Scanning script exited with error: " << sm_err << endl;
            err_end = 10;
        } else {
            cout << "Scanning script exited properly. " << endl;
        }
    } else if (argc == 4) {
        Scaning_Macro scanning_macro(stoi(argv[1]), stoi(argv[2]), stoi(argv[3]));
        int sm_err = scanning_macro.sm_run_cli();
        if (sm_err) {
            cout << "Scanning script exited with error: " << sm_err << endl;
            err_end = 10;
        } else {
            cout << "Scanning script exited properly. " << endl;
        }
    } else {
        cout << "Wrong argument! Point the config file." << endl;

        err_end = 1;
    }
    return end_program(err_end);
}

int end_program(int err_code)
{
    // measure time of program operation
	cout << "\n\nProgram duration: "
			<< (static_cast<double>(clock()) / CLOCKS_PER_SEC) << endl;
	cout << "\n\nProgram terminated." << endl;

    return err_code;
}