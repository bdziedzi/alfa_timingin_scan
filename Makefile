# -*- MakeFile -*-

PROJECT_NAME     := ALFA_timing_in
TARGETS          := alfa_timing_in
OUTPUT_DIRECTORY := _build
SHELL := /bin/bash

SDK_ROOT := ../SDK
PROJ_DIR := .
ROOT_CONFIG := `root-config --cflags --glibs`

# Source files common to all targets
SRC_FILES += \
	$(PROJ_DIR)/src/main.cpp \
	$(PROJ_DIR)/src/macros/scaning_macro.cpp \
	$(PROJ_DIR)/src/macros/support_functions.cpp


# Include folders common to all targets
INC_FOLDERS += \
	$(PROJ_DIR)/src \

# Optimization flags
OPT = -O3 -g3

# C flags common to all targets
CFLAGS += $(OPT)
# CFLAGS += -ftime-report

default: 
# g++ $(CFLAGS) -I $(INC_FOLDERS) $(SRC_FILES) $(ROOT_CONFIG)
	g++ -I $(INC_FOLDERS) $(SRC_FILES) $(ROOT_CONFIG)

clear: 
	rm -rf a.out